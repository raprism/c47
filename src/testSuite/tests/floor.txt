;*************************************************************
;*************************************************************
;**                                                         **
;**                          floor                          **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RMODE=0 IM=2compl SS=4 WS=64
Func: fnFloor



;=======================================
; floor(Long Integer) --> Long Integer
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"5" RX=LonI:"5"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"3605"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"3605" RX=LonI:"3605"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"-3595"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"-3595" RX=LonI:"-3595"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"3600000000000000000000000000000000000000000000000000000000000000000000005"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"3600000000000000000000000000000000000000000000000000000000000000000000005" RX=LonI:"3600000000000000000000000000000000000000000000000000000000000000000000005"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"-3599999999999999999999999999999999999999999999999999999999999999999999995"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"-3599999999999999999999999999999999999999999999999999999999999999999999995" RX=LonI:"-3599999999999999999999999999999999999999999999999999999999999999999999995"



;=======================================
; floor(Time)
;=======================================



;=======================================
; floor(Date)
;=======================================



;=======================================
; floor(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; floor(Real Matrix)
;=======================================
In:  AM=RAD FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,3[1.1,-2.3,3.5,-5.7,7.8,-8.9]"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ReMa:"M2,3[1.1,-2.3,3.5,-5.7,7.8,-8.9]" RX=ReMa:"M2,3[1,-3,3,-6,7,-9]"



;=======================================
; floor(Complex Matrix) --> Error24
;=======================================



;=======================================
; floor(Short Integer) --> Short Integer
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=ShoI:"5#7"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ShoI:"5#7" RX=ShoI:"5#7"



;=======================================
; floor(Real) --> Long Integer
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"0.0001"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.0001" RX=Real:"0"

In:  AM=GRAD FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"50"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"50" Rx=Real:"50"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"89.99999"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"89.99999" Rx=Real:"89"

In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"5.32564":DMS
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"5.32564":DMS Rx=Real:"5"

In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-5.32564":GRAD
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-5.32564":GRAD Rx=Real:"-6"

; NaN
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"NaN"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"NaN"

In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"NaN":RAD
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"NaN":RAD

; Infinity
In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=1 RX=Real:"inf"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RX=Real:"inf" RL=Real:"inf"

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"inf"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"inf"

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"-inf"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-inf"

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=1 RX=Real:"inf":DEG
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RX=Real:"inf" RL=Real:"inf":DEG

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"inf":MULTPI
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"inf":MULTPI

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"-inf":RAD
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-inf":RAD



;=======================================
; floor(Complex) --> Error24
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"1.1 i 2.4"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RX=Cplx:"1 i 2"
