// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

#include "c47.h"

#pragma GCC diagnostic ignored "-Wunused-parameter"

void audioTone(uint32_t frequency) {
}

void fnSetVolume(uint16_t volume) {
}

uint16_t getBeepVolume(void) {
  return NOVAL;
}

void fnGetVolume(uint16_t unusedButMandatoryParameter) {
}

void fnVolumeUp(uint16_t unusedButMandatoryParameter) {
}

void fnVolumeDown(uint16_t unusedButMandatoryParameter) {
}

void fnBuzz(uint16_t unusedButMandatoryParameter) {
}

void fnPlay(uint16_t unusedButMandatoryParameter) {
}