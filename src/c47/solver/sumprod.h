// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file sumprod.h
 ***********************************************/
#if !defined(SUMPROD_H)
  #define SUMPROD_H

  void showProgressReal(const real_t *a, real_t *ai, bool_t cpx);
  void fnProgrammableSum    (uint16_t label);
  void fnProgrammableProduct(uint16_t label);
#endif // !SUMPROD_H
