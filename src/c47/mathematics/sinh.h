// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file sinh.h
 ***********************************************/
#if !defined(SINH_H)
  #define SINH_H

  void fnSinh   (uint16_t unusedButMandatoryParameter);

#endif // !SINH_H
