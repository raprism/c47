// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file cube.h
 ***********************************************/
#if !defined(CUBE_H)
  #define CUBE_H

  void fnCube   (uint16_t unusedButMandatoryParameter);
#endif // !CUBE_H
