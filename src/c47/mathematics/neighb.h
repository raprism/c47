// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file neighb.h
 ***********************************************/
#if !defined(NEIGHB_H)
  #define NEIGHB_H

  void fnNeighb(uint16_t unusedButMandatoryParameter);
#endif // !NEIGHB_H
