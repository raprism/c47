// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file reToCx.h
 ***********************************************/
#if !defined(RETOCX_H)
  #define RETOCX_H

  void fnReToCx(uint16_t unusedButMandatoryParameter);
#endif // !RETOCX_H
