// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file bn.h
 ***********************************************/
#if !defined(BN_H)
  #define BN_H

  void fnBn    (uint16_t unusedButMandatoryParameter);
  void fnBnStar(uint16_t unusedButMandatoryParameter);
#endif // !BN_H
