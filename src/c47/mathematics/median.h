// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file mean.h
 ***********************************************/
#if !defined(MEDIAN_H)
  #define MEDIAN_H

  void fnPercentileXY       (uint16_t unusedButMandatoryParameter);
  void fnMedianXY           (uint16_t unusedButMandatoryParameter);
  void fnLowerQuartileXY    (uint16_t unusedButMandatoryParameter);
  void fnUpperQuartileXY    (uint16_t unusedButMandatoryParameter);
  void fnMADXY              (uint16_t unusedButMandatoryParameter);
  void fnIQRXY              (uint16_t unusedButMandatoryParameter);

#endif // !MEDIAN_H
