// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file shiftDigits.h
 ***********************************************/
#if !defined(SHIFTDIGITS_H)
  #define SHIFTDIGITS_H

  void fnSdr(uint16_t numberOfShifts);
  void fnSdl(uint16_t numberOfShifts);
#endif // !SHIFTDIGITS_H
