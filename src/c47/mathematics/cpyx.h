// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file cpyx.h
 ***********************************************/
#if !defined(CPYX_H)
  #define CPYX_H

  void fnCyx(uint16_t unusedButMandatoryParameter);
  void fnPyx(uint16_t unusedButMandatoryParameter);

  void logCyxReal(real_t *y, real_t *x, real_t *result, realContext_t *realContext);

#endif // !CPYX_H
