// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file ixyz.h
 ***********************************************/
#if !defined(IXYZ_H)
  #define IXYZ_H

  void fnIxyz(uint16_t unusedButMandatoryParameter);
#endif // !IXYZ_H
