// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file expt.h
 ***********************************************/
#if !defined(EXPT_H)
  #define EXPT_H

  void fnExpt   (uint16_t unusedButMandatoryParameter);
#endif // !EXPT_H
