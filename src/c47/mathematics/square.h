// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file square.h
 ***********************************************/
#if !defined(SQUARE_H)
  #define SQUARE_H

  void fnSquare   (uint16_t unusedButMandatoryParameter);

#endif // !SQUARE_H
