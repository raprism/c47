// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file w_inverse.h
 ***********************************************/
#if !defined(W_INVERSE_H)
  #define W_INVERSE_H

  void fnWinverse(uint16_t unusedButMandatoryParameter);
#endif // !W_INVERSE_H
