// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file arctanh.h
 ***********************************************/
#if !defined(ARCTANH_H)
  #define ARCTANH_H

  void fnArctanh   (uint16_t unusedButMandatoryParameter);
#endif // !ARCTANH_H
