// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file deltaPercentXmean.h
 ***********************************************/
#if !defined(DELTAPERCENTXMEAN_H)
  #define DELTAPERCENTXMEAN_H

  void fnDeltaPercentXmean(uint16_t unusedButMandatoryParameter);
  bool_t deltaPercentXmeanReal(real_t *xReal, real_t *rReal, realContext_t *realContext);

#endif // !DELTAPERCENTXMEAN_H
