// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/**
 * \file mathematics/log2.h
 */
#if !defined(LOG2_H)
  #define LOG2_H

  void  fnLog2   (uint16_t unusedButMandatoryParameter);
#endif // !LOG2_H
