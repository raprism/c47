// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file percentSigmaDeltaPercentXmean.h
 ***********************************************/
#if !defined(PCSIGMADPCMEAN_H)
  #define PCSIGMADPCMEAN_H

  void fnPcSigmaDeltaPcXmean(uint16_t unusedButMandatoryParameter);
#endif // !PCSIGMADPCMEAN_H
