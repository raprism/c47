// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file ceil.h
 ***********************************************/
#if !defined(CEIL_H)
  #define CEIL_H

  void fnCeil   (uint16_t unusedButMandatoryParameter);
#endif // !CEIL_H
