// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file dblMultiplication.h
 ***********************************************/
#if !defined(DBLMULTIPLICATION_H)
  #define DBLMULTIPLICATION_H

  void fnDblMultiply(uint16_t unusedButMandatoryParameter);
#endif // !DBLMULTIPLICATION_H
