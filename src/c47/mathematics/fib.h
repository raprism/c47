// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file fib.h
 ***********************************************/
#if !defined(FIB_H)
  #define FIB_H

  void fnFib   (uint16_t unusedButMandatoryParameter);
#endif // !FIB_H
