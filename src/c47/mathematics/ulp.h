// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file ulp.h
 ***********************************************/
#if !defined(ULP_H)
  #define ULP_H

  void fnUlp(uint16_t unusedButMandatoryParameter);
#endif // !ULP_H
