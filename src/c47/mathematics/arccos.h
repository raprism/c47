// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file arccos.h
 ***********************************************/
#if !defined(ARCCOS_H)
  #define ARCCOS_H

  void fnArccos   (uint16_t unusedButMandatoryParameter);

#endif // !ARCCOS_H
