// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file percentMRR.h
 ***********************************************/
#if !defined(PERCENTMRR_H)
  #define PERCENTMRR_H

  void fnPercentMRR(uint16_t unusedButMandatoryParameter);
#endif // !PERCENTMRR_H
