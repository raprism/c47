// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file percentT.h
 ***********************************************/
#if !defined(DELTAPERCENT_H)
  #define DELTAPERCENT_H

  void fnDeltaPercent(uint16_t unusedButMandatoryParameter);
  bool_t deltaPercentXmeanReal(real_t *xReal, real_t *rReal, realContext_t *realContext);
#endif // !DELTAPERCENT_H
