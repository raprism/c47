// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file dblDivision.h
 ***********************************************/
#if !defined(DBLDIVISION_H)
  #define DBLDIVISION_H

  void fnDblDivide         (uint16_t unusedButMandatoryParameter);
  void fnDblDivideRemainder(uint16_t unusedButMandatoryParameter);
#endif // !DBLDIVISION_H
