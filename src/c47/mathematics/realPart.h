// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file realPart.h
 ***********************************************/
#if !defined(REALPART_H)
  #define REALPART_H

  void fnRealPart   (uint16_t unusedButMandatoryParameter);
#endif // !REALPART_H
