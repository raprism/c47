// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/**
 * \file ui/tone.h
 */
#if !defined(TONE_H)
  #define TONE_H

  void fnTone(uint16_t toneNum);
  void fnBeep(uint16_t unusedButMandatoryParameter);
#endif // .TONE_H
