// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file gev.h
 ***********************************************/
#if !defined(GEV_H)
  #define GEV_H

  void fnGEVP                      (uint16_t unusedButMandatoryParameter);
  void fnGEVL                      (uint16_t unusedButMandatoryParameter);
  void fnGEVR                      (uint16_t unusedButMandatoryParameter);
  void fnGEVI                      (uint16_t unusedButMandatoryParameter);
#endif // !GEV_H
