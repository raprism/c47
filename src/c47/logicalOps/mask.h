// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file mask.h
 ***********************************************/
#if !defined(MASK_H)
  #define MASK_H

  void fnMaskl(uint16_t numberOfBits);
  void fnMaskr(uint16_t numberOfBits);
#endif // !MASK_H
