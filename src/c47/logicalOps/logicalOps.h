// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file logicalOps.h
 ***********************************************/
#if !defined(LOGICALOPS_H)
  #define LOGICALOPS_H

  #include "and.h"
  #include "countBits.h"
  #include "mask.h"
  #include "nand.h"
  #include "nor.h"
  #include "not.h"
  #include "or.h"
  #include "rotateBits.h"
  #include "setClearFlipBits.h"
  #include "xnor.h"
  #include "xor.h"
#endif // !LOGICALOPS_H
