// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file countBits.h
 ***********************************************/
#if !defined(COUNTBITS_H)
  #define COUNTBITS_H

  void fnCountBits(uint16_t unusedButMandatoryParameter);
  void countBitsError    (void);
  void countBitsShoI     (void);
#endif // !COUNTBITS_H
