// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

#if !defined(C47EXTENSIONS_H)
#define C47EXTENSIONS_H

  #include "addons.h"
  #include "graphs.h"
  #include "graphText.h"
  #include "inlineTest.h"
  #include "jm.h"
  #include "keyboardTweak.h"
  #include "radioButtonCatalog.h"
  #include "textfiles.h"
  #include "xeqm.h"

#endif // .C47EXTENSIONS_H
