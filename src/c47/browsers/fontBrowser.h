// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors
#if !defined(FONTBROWSER_H)
  #define FONTBROWSER_H

  #if !defined(TESTSUITE_BUILD)
    void initFontBrowser(void);
    void fontBrowser    (uint16_t unusedButMandatoryParameter);
  #endif // !TESTSUITE_BUILD
#endif // !FONTBROWSER_H
