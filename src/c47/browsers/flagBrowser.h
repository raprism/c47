// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors
#if !defined(FLAGBROWSER_H)
#define FLAGBROWSER_H

  void flagBrowser(uint16_t init); //JM
#endif // !FLAGBROWSER_H
